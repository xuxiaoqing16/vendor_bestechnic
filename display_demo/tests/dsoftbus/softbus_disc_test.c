/*
 * Copyright (c) 2023 Bestechnic (Shanghai) Co., Ltd. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "softbus_disc_test.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "discovery_service.h"
#include "softbus_common.h"
#include "softbus_errcode.h"
#include "softbus_server_frame.h"

#define DISC_TEST_PKG_NAME "DISC_TEST"
#define MAX_TEST_COUNT 5
#define TEST_COUNT_INTREVAL 5

static int GetPublishId(void)
{
    static int32_t publishId = 0;
    publishId++;
    return publishId;
}

static PublishInfo g_pInfo = {
    .publishId = 1,
    .mode = DISCOVER_MODE_ACTIVE,
    .medium = COAP,
    .freq = MID,
    .capability = "dvKit",
    .capabilityData = NULL,
    .dataLen = 0
};

static void OnPublishSuccess(int publishId)
{
    printf(">>>OnPublishSuccess publishId = %d\n", publishId);
}

static void OnPublishFail(int publishId, PublishFailReason reason)
{
    printf(">>>OnPublishFail publishId = %d, reason = %d\n", publishId, reason);
}

static IPublishCallback g_publishCb = {
    .OnPublishSuccess = OnPublishSuccess,
    .OnPublishFail = OnPublishFail
};

static void TestActivePubSrv(void)
{
    g_pInfo.publishId = GetPublishId();
    if (PublishService(DISC_TEST_PKG_NAME, &g_pInfo, &g_publishCb) != SOFTBUS_OK) {
        printf("test failed, [%s].\n", __FUNCTION__);
    }
}

static void TestUnPubSrv(void)
{
    if (UnPublishService(DISC_TEST_PKG_NAME, g_pInfo.publishId) != SOFTBUS_OK) {
        printf("test failed, [%s].\n", __FUNCTION__);
    }
}

void SoftbusDiscTest(void)
{
    uint32_t testCnt = 0;
    for (testCnt = 0; testCnt < MAX_TEST_COUNT; ++testCnt) {
        printf("SoftbusDiscTest: %u / %u\n", testCnt, MAX_TEST_COUNT);
        TestActivePubSrv();
        sleep(TEST_COUNT_INTREVAL);
        TestUnPubSrv();
    }
}
